<?php

namespace Acme\ToDoBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Acme\ToDoBundle\Document\Item;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class ToDoRestController extends FOSRestController
{

    public function getItemsAction()
    {
        // var_dump(expression)
        $repository = $this->get('doctrine_mongodb')
                    ->getManager()
                    ->getRepository('AcmeToDoBundle:Item');
        $all = $repository->findAll();

        $view = $this->view(iterator_to_array($all));
        return $this->handleView($view);
    }

    public function postItemAction()
    {
        $request = $this->getRequest();
        $post = json_decode($request->getContent(), true);
        $item = new Item();
        $item->setName($post['name']);
        $item->setStatus(false);

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($item);
        $dm->flush();


        $view = $this->view($item);
        return $this->handleView($view);
    }

    public function getItemAction($id)
    {
        $repository = $this->get('doctrine_mongodb')
                    ->getManager()
                    ->getRepository('AcmeToDoBundle:Item');

        $item = $repository->findOneById($id);

        if (!$item) {
            throw $this->createNotFoundException('No item found for id '.$id);
        }
        $view = $this->view($item);
        return $this->handleView($view);

    }


    public function putItemAction($id)
    {
        $request = $this->getRequest();
        $data = json_decode($request->getContent(), true);

        $repository = $this->get('doctrine_mongodb')
                    ->getManager()
                    ->getRepository('AcmeToDoBundle:Item');

        $item = $repository->findOneById($id);
        $item->setName($data['name']);
        $item->setStatus($data['status']);
        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($item);
        $dm->flush();
        $view = $this->view($item);
        return $this->handleView($view);

    }

    public function deleteItemAction($id)
    {
        $repository = $this->get('doctrine_mongodb')
                    ->getManager()
                    ->getRepository('AcmeToDoBundle:Item');

        $item = $repository->findOneById($id);
        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->remove($item);
        $dm->flush();
        $view = $this->view($item);
        return $this->handleView($view);

    }
}
